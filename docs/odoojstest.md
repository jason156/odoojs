
## odoo jsonrpc 增强模块

https://gitee.com/odoowww/odoo-patch/tree/master/addons/ow_json

## odoojs 源码
https://gitee.com/odoowww/odoojs/tree/master/packages/odoojs-core


## React + antd + umi + dva + odoojs 例子
https://gitee.com/odoowww/account

## 使用方法
* 下载模块 ow_json
* 在 odoo 启动配置文件中的 server_wide_modules 配置项中 添加 此模块名 ow_json
* 运行odoo
* 前端 安装 odoojs-core
* 按照 odoojs-core 的 readme 的指导 写前端代码


