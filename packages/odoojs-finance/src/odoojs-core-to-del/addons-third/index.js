import ow_account from './ow_account';
import ow_account_balance from './ow_account_balance';
import l10n_cn_hr_payroll from './l10n_cn_hr_payroll';
import om_account_asset from './om_account_asset';

export default {
  ow_account,
  ow_account_balance,
  l10n_cn_hr_payroll,
  om_account_asset,
};
