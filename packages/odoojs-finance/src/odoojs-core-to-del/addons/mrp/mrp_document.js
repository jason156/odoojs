export default {
  models: {
    'mrp.document': {
      fields: ['ir_attachment_id', 'active', 'priority'],
    },
  },
};
