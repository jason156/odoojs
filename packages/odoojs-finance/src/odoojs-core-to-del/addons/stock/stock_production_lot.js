export default {
  models: {
    'stock.production.lot': {
      fields: [
        'name',
        'ref',
        'product_id',
        'product_uom_id',
        'quant_ids',
        'product_qty',
      ],
    },
  },
};
