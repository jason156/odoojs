export default {
  models: {
    'account.fiscal.year': {
      fields: ['name', 'date_from', 'date_to', 'company_id'],
    },
  },
};
