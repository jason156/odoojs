import accountMove from './accountMove';
import accountInvoice from './accountInvoice';
import accountPayment from './accountPayment';

export default {
  'account.move': accountMove,
  'account.invoice': accountInvoice,
  'account.payment': accountPayment,
};
