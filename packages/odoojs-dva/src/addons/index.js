import account from './account';
import sale from './sale';
import purchase from './purchase';
import stock from './stock';
import hr_payroll from './hr_payroll';

export default {
  account,
  sale,
  purchase,
  stock,
  hr_payroll,
};
