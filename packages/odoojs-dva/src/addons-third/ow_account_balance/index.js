import accountMove from './accountMove';
import accountBalance from './accountBalance';
import accountStock from './accountStock';

export default {
  'account.move': accountMove,
  'account.balance': accountBalance,
  'account.stock': accountStock,
};
